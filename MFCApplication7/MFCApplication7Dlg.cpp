﻿
// MFCApplication7Dlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFCApplication7.h"
#include "MFCApplication7Dlg.h"
#include "afxdialogex.h"
#include<time.h>
#include<iostream>
#include<stdlib.h>
#include<string.h>
#include<string>
#include"finishwin.h"
#include"resource.h"
#include<Windows.h>
#include<process.h>
#include<iomanip>
#include<MMSystem.h>
#pragma comment(lib,"Winmm.lib")


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCApplication7Dlg 对话框



CMFCApplication7Dlg::CMFCApplication7Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFCAPPLICATION7_DIALOG, pParent)
	, n12(_T(""))
	, n11(_T(""))
	, n13(_T(""))
	, n14(_T(""))
	, n21(_T(""))
	, n22(_T(""))
	, n23(_T(""))
	, n24(_T(""))
	, n31(_T(""))
	, n32(_T(""))
	, n33(_T(""))
	, n34(_T(""))
	, n41(_T(""))
	, n42(_T(""))
	, n43(_T(""))
	, n44(_T(""))
	, defen(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCApplication7Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT2, n12);
	DDX_Text(pDX, IDC_EDIT1, n11);
	DDX_Text(pDX, IDC_EDIT3, n13);
	DDX_Text(pDX, IDC_EDIT4, n14);
	DDX_Text(pDX, IDC_EDIT15, n21);
	DDX_Text(pDX, IDC_EDIT8, n22);
	DDX_Text(pDX, IDC_EDIT6, n23);
	DDX_Text(pDX, IDC_EDIT5, n24);
	DDX_Text(pDX, IDC_EDIT17, n31);
	DDX_Text(pDX, IDC_EDIT12, n32);
	DDX_Text(pDX, IDC_EDIT9, n33);
	DDX_Text(pDX, IDC_EDIT7, n34);
	DDX_Text(pDX, IDC_EDIT18, n41);
	DDX_Text(pDX, IDC_EDIT16, n42);
	DDX_Text(pDX, IDC_EDIT13, n43);
	DDX_Text(pDX, IDC_EDIT10, n44);
	DDX_Control(pDX, IDC_EDIT14, score);
	DDX_Text(pDX, IDC_EDIT11, defen);
	DDX_Control(pDX, IDC_EDIT19, title);
}

BEGIN_MESSAGE_MAP(CMFCApplication7Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON2, &CMFCApplication7Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &CMFCApplication7Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON4, &CMFCApplication7Dlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON3, &CMFCApplication7Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON5, &CMFCApplication7Dlg::OnBnClickedButton5)
	ON_EN_CHANGE(IDC_EDIT11, &CMFCApplication7Dlg::OnEnChangeEdit11)
	ON_EN_CHANGE(IDC_EDIT9, &CMFCApplication7Dlg::OnEnChangeEdit9)
END_MESSAGE_MAP()


// CMFCApplication7Dlg 消息处理程序
CString bianhuan(int n)
{
	CString str;
	str.Format(_T("%d"), n);
	return str;

}
void zhaoling(int arr[][4],int *p[16],int num)
{
	 
	num = 0;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr[i][j] ==0) {
				p[num] =& arr[i][j];
				num++;

			}

		}
	}
	if(num==0){
		
		finishwin dlg;
		dlg.DoModal();
		

	}
	srand(time(0));
	int n = rand() % num;
	int m = rand() % 3;
	if ((m == 0 )||( m == 1))
		*p[n] = 2;
	else
		*p[n] = 4;
	

}
void swap(int &a,int &b)
{
	int temp=a;

	a = b;
	b = temp;

}
void bijiao(int& a, int& b)
{
	if (a != 0 && b == 0)swap(a,b);
	if (a == b) {
		b *= 2; a = 0;
	}

}
int he(int arr[][4])
{
	int sum=0;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			sum += 5 * arr[i][j];
		}
	}
	return sum;

}

BOOL CMFCApplication7Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	PlaySound((LPCTSTR)IDR_WAVE1,AfxGetInstanceHandle(),SND_RESOURCE|SND_ASYNC);
	n11 = TEXT(" "); n12 = TEXT(" "); n13 = TEXT(" "); n14 = TEXT(" ");
	n21 = TEXT(" "); n22 = TEXT(" "); n23 = TEXT(" "); n24 = TEXT(" ");
	n31 = TEXT(" "); n32 = TEXT(" "); n33 = TEXT(" "); n34 = TEXT(" ");
	n41 = TEXT(" "); n42 = TEXT(" "); n43 = TEXT(" "); n44 = TEXT(" ");
	score.SetWindowTextW(TEXT("SCORE"));
	titletype.CreatePointFont(300, _T("隶书"));
	title.SetFont(&titletype);
	title.SetWindowTextW(TEXT("2048小游戏"));
	
	
	

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCApplication7Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCApplication7Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCApplication7Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCApplication7Dlg::OnBnClickedButton2()
{
	for (int i = 0; i <=3; i++)
	{
		bijiao(arr[1][i], arr[0][i]);
	}
	for (int i = 0; i <= 3; i++)
	{
		bijiao(arr[2][i], arr[1][i]);
	}
	for (int i = 0; i <= 3; i++)
	{
		bijiao(arr[1][i], arr[0][i]);
	}
	for (int i = 0; i <= 3; i++)
	{
		bijiao(arr[3][i], arr[2][i]);
	}
	for (int i = 0; i <= 3; i++)
	{
		bijiao(arr[2][i], arr[1][i]);

	}
	for (int i = 0; i <= 3; i++)
	{
		bijiao(arr[1][i], arr[0][i]);
	}
	zhaoling(arr, p, num);
	n11 = bianhuan(arr[0][0]);
	n12 = bianhuan(arr[0][1]);
	n13 = bianhuan(arr[0][2]);
	n14 = bianhuan(arr[0][3]);
	n21 = bianhuan(arr[1][0]);
	n22 = bianhuan(arr[1][1]);
	n23 = bianhuan(arr[1][2]);
	n24 = bianhuan(arr[1][3]);
	n31 = bianhuan(arr[2][0]);
	n32 = bianhuan(arr[2][1]);
	n33 = bianhuan(arr[2][2]);
	n34 = bianhuan(arr[2][3]);
	n41 = bianhuan(arr[3][0]);
	n42 = bianhuan(arr[3][1]);
	n43 = bianhuan(arr[3][2]);
	n44 = bianhuan(arr[3][3]);
	defen = bianhuan(he(arr));
	UpdateData(FALSE);
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCApplication7Dlg::OnBnClickedButton1()
{
	
	zhaoling(arr, p, num);
		n11 = bianhuan(arr[0][0]);
		n12 = bianhuan(arr[0][1]);
		n13 = bianhuan(arr[0][2]);
		n14 = bianhuan(arr[0][3]);
		n21 = bianhuan(arr[1][0]);
		n22 = bianhuan(arr[1][1]);
		n23 = bianhuan(arr[1][2]);
		n24 = bianhuan(arr[1][3]);
		n31 = bianhuan(arr[2][0]);
		n32 = bianhuan(arr[2][1]);
		n33 = bianhuan(arr[2][2]);
		n34 = bianhuan(arr[2][3]);
		n41 = bianhuan(arr[3][0]);
		n42 = bianhuan(arr[3][1]);
		n43 = bianhuan(arr[3][2]);
		n44 = bianhuan(arr[3][3]);
		defen = bianhuan(he(arr));
		UpdateData(FALSE);
	
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCApplication7Dlg::OnBnClickedButton4()
{
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[2][i], arr[3][i]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[1][i], arr[2][i]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[2][i], arr[3][i]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[0][i], arr[1][i]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[1][i], arr[2][i]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[2][i], arr[3][i]);
	}
	zhaoling(arr, p, num);
	n11 = bianhuan(arr[0][0]);
	n12 = bianhuan(arr[0][1]);
	n13 = bianhuan(arr[0][2]);
	n14 = bianhuan(arr[0][3]);
	n21 = bianhuan(arr[1][0]);
	n22 = bianhuan(arr[1][1]);
	n23 = bianhuan(arr[1][2]);
	n24 = bianhuan(arr[1][3]);
	n31 = bianhuan(arr[2][0]);
	n32 = bianhuan(arr[2][1]);
	n33 = bianhuan(arr[2][2]);
	n34 = bianhuan(arr[2][3]);
	n41 = bianhuan(arr[3][0]);
	n42 = bianhuan(arr[3][1]);
	n43 = bianhuan(arr[3][2]);
	n44 = bianhuan(arr[3][3]);
	defen = bianhuan(he(arr));
	UpdateData(FALSE);
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCApplication7Dlg::OnBnClickedButton3()
{
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][1], arr[i][0]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][2], arr[i][1]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][1], arr[i][0]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][3], arr[i][2]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][2], arr[i][1]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][1], arr[i][0]);
	}
	zhaoling(arr, p, num);
	n11 = bianhuan(arr[0][0]);
	n12 = bianhuan(arr[0][1]);
	n13 = bianhuan(arr[0][2]);
	n14 = bianhuan(arr[0][3]);
	n21 = bianhuan(arr[1][0]);
	n22 = bianhuan(arr[1][1]);
	n23 = bianhuan(arr[1][2]);
	n24 = bianhuan(arr[1][3]);
	n31 = bianhuan(arr[2][0]);
	n32 = bianhuan(arr[2][1]);
	n33 = bianhuan(arr[2][2]);
	n34 = bianhuan(arr[2][3]);
	n41 = bianhuan(arr[3][0]);
	n42 = bianhuan(arr[3][1]);
	n43 = bianhuan(arr[3][2]);
	n44 = bianhuan(arr[3][3]);
	defen = bianhuan(he(arr));
	UpdateData(FALSE);
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCApplication7Dlg::OnBnClickedButton5()
{
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][2], arr[i][3]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][1], arr[i][2]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][2], arr[i][3]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][0], arr[i][1]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][1], arr[i][2]);
	}
	for (int i = 0; i < 4; i++)
	{
		bijiao(arr[i][2], arr[i][3]);
	}
	zhaoling(arr, p, num);
	n11 = bianhuan(arr[0][0]);
	n12 = bianhuan(arr[0][1]);
	n13 = bianhuan(arr[0][2]);
	n14 = bianhuan(arr[0][3]);
	n21 = bianhuan(arr[1][0]);
	n22 = bianhuan(arr[1][1]);
	n23 = bianhuan(arr[1][2]);
	n24 = bianhuan(arr[1][3]);
	n31 = bianhuan(arr[2][0]);
	n32 = bianhuan(arr[2][1]);
	n33 = bianhuan(arr[2][2]);
	n34 = bianhuan(arr[2][3]);
	n41 = bianhuan(arr[3][0]);
	n42 = bianhuan(arr[3][1]);
	n43 = bianhuan(arr[3][2]);
	n44 = bianhuan(arr[3][3]);
	defen=bianhuan(he(arr));
	UpdateData(FALSE);
	// TODO: 在此添加控件通知处理程序代码
}


void CMFCApplication7Dlg::OnEnChangeEdit11()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
}


void CMFCApplication7Dlg::OnEnChangeEdit9()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
}
