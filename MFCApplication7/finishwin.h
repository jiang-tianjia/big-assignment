﻿#pragma once


// finishwin 对话框

class finishwin : public CDialogEx
{
	DECLARE_DYNAMIC(finishwin)

public:
	finishwin(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~finishwin();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	
	
	afx_msg void OnBnClickedOk();
	afx_msg void OnEnChangeEdit1();

};