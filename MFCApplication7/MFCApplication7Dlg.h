﻿
// MFCApplication7Dlg.h: 头文件
//

#pragma once


// CMFCApplication7Dlg 对话框
class CMFCApplication7Dlg : public CDialogEx
{
// 构造
public:
	CMFCApplication7Dlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCAPPLICATION7_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
	
	CString n12;
	CString n11;
	CString n13;
	CString n14;
	CString n21;
	CString n22;
	CString n23;
	CString n24;
	CString n31;
	CString n32;
	CString n33;
	CString n34;
	CString n41;
	CString n42;
	CString n43;
	CString n44;
	afx_msg void OnBnClickedButton1();
	int arr[4][4] = {0};
	int* p[16];
	int num;
	int sum;
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton5();
private:
	CEdit score;
	CString defen;
	
public:
	afx_msg void OnEnChangeEdit11();
	afx_msg void OnEnChangeEdit9();
private:
	CEdit title;
	CFont titletype;
};
