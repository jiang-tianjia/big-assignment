﻿// finishwin.cpp: 实现文件
//

#include "pch.h"
#include "MFCApplication7.h"
#include "finishwin.h"
#include "afxdialogex.h"


// finishwin 对话框

IMPLEMENT_DYNAMIC(finishwin, CDialogEx)

finishwin::finishwin(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{
	
}

finishwin::~finishwin()
{
}

void finishwin::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	
}


BEGIN_MESSAGE_MAP(finishwin, CDialogEx)
	ON_BN_CLICKED(IDOK, &finishwin::OnBnClickedOk)
	ON_EN_CHANGE(IDC_EDIT1, &finishwin::OnEnChangeEdit1)
END_MESSAGE_MAP()


// finishwin 消息处理程序


void finishwin::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	
	CDialogEx::OnOK();
	exit (0);
}


void finishwin::OnEnChangeEdit1()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
	
	// TODO:  在此添加控件通知处理程序代码
}
